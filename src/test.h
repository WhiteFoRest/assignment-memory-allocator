#ifndef TEST_H
#define TEST_H

#include <inttypes.h>

#define HEAP_SIZE 8192
#define NUMBER_OF_TESTS 5

void test1(void* start_heap);
void test2(void* start_heap);
void test3(void* start_heap);
void test4(void* start_heap);
void test5(void* start_heap);

void before_test(uint8_t number, void* start_heap);
void after_test(uint8_t number, void* start_heap);

int tests_start();

#endif
