#define _DEFAULT_SOURCE
#include "test.h"
#include "util.h"
#include "mem.h"
#include "mem_internals.h"
#include <unistd.h>

static struct block_header* block_get_header(void* content) {
    return (struct block_header*) (((uint8_t*)content)-offsetof(struct block_header, contents));
}

void test1(void* start_heap) {
    size_t content_capacity = 256;
    void* new_content = _malloc(content_capacity);
    if (!new_content) {
        err("Test №1 failed: _malloc return null\n");
    }

    debug("Heap after _malloc:\n");
    debug_heap(stdout, start_heap);

    struct block_header* header = block_get_header(new_content);
    if (header->is_free) {
        err("Test №1 failed: not set false to is_free\n");
    }
    if (header->capacity.bytes < content_capacity) {
        err("Test №1 failed: alloc too little block\n");
    }
    if (header->capacity.bytes < HEAP_SIZE) {
        if (!header->next || !header->next->is_free) {
            err("Test #1 failed: blocks didn't split, but may do it\n");
        }
    }

    debug("Memory allocated corrected!\n");
    _free(new_content);
}

void test2(void* start_heap) {
    size_t content_capacity = 256;

    void* first_content = _malloc(content_capacity);
    void* second_content = _malloc(content_capacity);
    void* third_content = _malloc(content_capacity);

    struct block_header* first_header = block_get_header(first_content);
    struct block_header* second_header = block_get_header(second_content);
    struct block_header* third_header = block_get_header(third_content);

    if (!first_content || !second_content || !third_content) {
        err("Test №2 failed: _malloc return null\n");
    }

    debug("Alloc successful!\n Heap after _malloc:\n");
    debug_heap(stdout, start_heap);

    _free(second_content);

    debug("Heap after _free:\n");
    debug_heap(stdout, start_heap);

    if (!second_header->is_free) {
        err("Test №2 failed: second block not freed!\n");
    }
    if (first_header->is_free || third_header->is_free) {
        err("Test №2 failed: first|third block freed!\n");
    }

    debug("Memory free corrected!\n");
    _free(third_content);
    _free(first_content);
}

size_t heap_size(void const* heap) {
  struct block_header* block = (struct block_header*)heap;
  size_t sz = 0;
  while(block){
    sz += size_from_capacity(block->capacity).bytes;
    block = block->next;
  }
  return sz;
}

void test3(void* start_heap) {
    size_t content_capacity = 256;

    void* first_content = _malloc(content_capacity);
    void* second_content = _malloc(content_capacity);
    void* third_content = _malloc(heap_size(start_heap) + content_capacity);

    struct block_header* first_header = block_get_header(first_content);
    struct block_header* second_header = block_get_header(second_content);
    struct block_header* third_header = block_get_header(third_content);

    if (!first_content || !second_content || !third_content) {
        err("Test №3 failed: _malloc return null\n");
    }

    debug("Alloc successful!\n Heap after _malloc:\n");
    debug_heap(stdout, start_heap);

    _free(second_content);
    _free(first_content);

    debug("Heap after _free:\n");
    debug_heap(stdout, start_heap);

    if (!first_header->is_free) {
        err("Test №3 failed: first block not freed!\n");
    }
    if (third_header->is_free) {
        err("Test №3 failed: third block freed!\n");
    }
    if ((first_header->next == second_header) || (first_header->next != third_header)) {
        err("Test №3 failed: first and second block not correct merged!\n");
    }

    debug("Memory free corrected!\n");
    _free(third_content);
}

static void* void_pointer_plus(void* pointer, int64_t summand) {
    return (void*) ((uint8_t*) pointer + summand);
}

void test4(void* start_heap) {
    size_t content_capacity = HEAP_SIZE + 16;
    void* new_content = _malloc(content_capacity);
    if (!new_content) {
        err("Test №4 failed: _malloc return null\n");
    }

    debug("Heap after _malloc:\n");
    debug_heap(stdout, start_heap);

    struct block_header* header = block_get_header(new_content);
    if (header->is_free) {
        err("Test №4 failed: not set false to is_free\n");
    }
    if (header->capacity.bytes < content_capacity) {
        err("Test №4 failed: alloc too little block\n");
    }
    if ((void *) header > void_pointer_plus(start_heap, HEAP_SIZE)) {
        err("Test №4 failed: block not start in old heap\n");
    }

    debug("Memory allocated corrected!\n");
    _free(new_content);
}

void test5(void* start_heap) {
    size_t sz = heap_size(start_heap);
	void* heap_end = (void*)((uint8_t*)HEAP_START + sz);
	void* alloc_trash_after_heap = map_pages(heap_end, 5000, MAP_FIXED_NOREPLACE);
	debug("Allocated trash after end of the heap: %p\n", alloc_trash_after_heap);
    size_t content_capacity = 32000;
    void* new_content = _malloc(content_capacity);
    if (!new_content) {
        err("Test №5 failed: _malloc return null\n");
    }

    debug("Heap after _malloc:\n");
    debug_heap(stdout, start_heap);

    struct block_header* header = block_get_header(new_content);
    if (header->is_free) {
        err("Test №5 failed: not set false to is_free\n");
    }
    if (header->capacity.bytes < content_capacity) {
        err("Test №5 failed: alloc too little block\n");
    }

    if ((void *) header < void_pointer_plus(start_heap, HEAP_SIZE * 3)) {
        err("Test №5 failed: block start in old heap\n");
    }

    debug("Memory allocated corrected!\n");
    _free(new_content);
}

void before_test(uint8_t number, void* start_heap) {
    debug("Start Test №%d...\n", number);
    debug("Heap before test:\n");
    debug_heap(stdout, start_heap);
    debug("The test is starting!\n");
}

static void (*test_func[NUMBER_OF_TESTS])(void*) = { test1, test2, test3, test4, test5 };

void after_test(uint8_t number, void* start_heap) {
    debug("Heap after test\n");
    debug_heap(stdout, start_heap);
    debug("Test №%d finished success!\n\n\n", number);
}

int tests_start(){
    void* heap_start = heap_init(HEAP_SIZE);
    if (heap_start == NULL)
        return 1;

    for (size_t i = 0; i < NUMBER_OF_TESTS; ++i) {
        before_test(i + 1, heap_start);
        test_func[i](heap_start);
        after_test(i + 1, heap_start);
    }
    debug("All tests are passed!\n");
    return 0;
}
