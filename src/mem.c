#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool block_is_big_enough( size_t query, struct block_header* block ){ 
  return block->capacity.bytes >= query;
}

/**
* @param mem Кол-во доступной памяти
* @return Кол-во страниц, которое может уместиться в памяти
*/
static size_t pages_count( size_t mem ){
  return mem / getpagesize() + (( mem % getpagesize() ) > 0);
}

/**
* @param mem Кол-во доступной памяти
* @return Кол-во памяти, которое можно замапить со страницами
*/
static size_t round_pages( size_t mem ){
  return getpagesize() * pages_count( mem );
}

/**
* Кастит указатель адреса начала блока к (struct block_header*) и кладет в него заполненную структуру
* @param addr Адрес начала блока, @param block_sz  Размер блока, @param next  Ссылка на следующий блок региона 
 */
static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

/**
 * @param query Запрашиваемый размер региона
 * @return Актуальный размер региона на основе сравнения кол-ва доступной памяти, которую можно замапить со страницами, и минимально возможного размера региона
 */
static size_t region_actual_size( size_t query ) {
  return size_max( round_pages( query ), REGION_MIN_SIZE );
}

extern inline bool region_is_invalid( const struct region* r );

/**
 * @param addr Адрес начала региона, @param length Размер региона, @param additional_flags Доп флаги для syscall
 * @return Указатель на начало региона
 */
void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

static struct region create_region(void* addr, const size_t size, bool extends) {
    return (struct region) {
        .addr = addr,
        .size = size,
        .extends = extends
    };
}

/**
 * @param addr Адрес с которого желательно, чтобы начинался регион, @param query Запрашиваемый размер региона
 * @return Ссылка на регион с актуальным размером
 */
static struct region alloc_region( void const * addr, size_t query ) {
  size_t actual_size = region_actual_size(query);
  void* addr_result = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);

  if (addr_result == MAP_FAILED) {
      addr_result = map_pages(addr, actual_size, 0);
      if (addr_result == MAP_FAILED)
        return REGION_INVALID;
  }

  block_init(
    addr_result,
    (block_size) {
      actual_size
    },
    NULL);
    
  return create_region(addr_result, actual_size, false);
}

static void* block_after( struct block_header const* block );

/**
 * @param initial Желаемый размер кучи
 * @return 1. Указатель на начало кучи (Если аллоцировали регион успешно)  
 * 2. NULL (Если не получилось аллоцировать регион)
 */
void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) )
    return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/**
 * Разделение блоков (если найденный свободный блок слишком большой )---
 * @param block Искомый блок, @param query_capacity Вместимость запрашиваемой блока
 * @return Можно разделить или нет?
 */
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static struct block_header* create_block(void* restrict addr, block_size size, void* restrict next ) {
  block_init(addr, size, next);
  return (struct block_header*) addr;
}

/**
 * @param block Искомый блок, @param query Запрашиваемый размер
 * @return Получилось разделить или нет?
 */
static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block_splittable(block, query))
    return false;

  block_size size_new_block = (block_size) {
    block->capacity.bytes - query
  };
  block->capacity.bytes = query;

  void* start_new_block = block_after(block);
  struct block_header* new_block = create_block(start_new_block, size_new_block, block->next);
  block->next = new_block;

  return true;
}

/*  --- Слияние соседних свободных блоков --- */

/**
 * Вычисление адреса начала следующего блока
 * @param block Рассматриваемый блок
 * @return Ссылка на начало следующего блока
 */
static void* block_after( struct block_header const* block ) {
  return (void*)(block->contents + block->capacity.bytes);
}

/**
 * @param fst Указатель на первый по порядку блок, @param snd Указатель на второй по порядку блок
 * @return Лежат ли блоки друг за другом в памяти?
 */
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

/**
 * @param fst Первый блок, @param snd Второй блок
 * @return Возможно ли слияние двух соседних блоков?
 */
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

/**
 * @param block Блок который нужно объединить со следующим
 * @return Статус объединения заданного блока со следующим за ним
 */
static bool try_merge_with_next( struct block_header* block ) {
  if (block->next != NULL && mergeable(block, block->next)) {
      block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
      block->next = block->next->next;
      return true;
  }

  return false;
}


/*  --- ... ecли размера кучи хватает --- */

/**
 * Результат поиска блока, который вмещает:
 * 1) Статус нахождения
 * 2) Сам блок(по возможности)
 */
struct block_search_result {
  enum status{
    BSR_FOUND_GOOD_BLOCK,
    BSR_REACHED_END_NOT_FOUND,
    BSR_CORRUPTED
    } type;
  struct block_header* block;
};

struct block_search_result block_search_result_init(struct block_header* restrict block, enum status type){
  return (struct block_search_result) {
      .type = type,
      .block = block
    };
}

/**
 * @param block Адрес начала связного списка блоков, @param size Интересуемый размер
 * @return Хороший или последний блок
 */
static struct block_search_result find_good_or_last( struct block_header* restrict block, size_t sz ) {
  
  if (block == NULL) {
    return block_search_result_init(block, BSR_REACHED_END_NOT_FOUND);
  }

  while (block->next != NULL) {
    if (block->is_free) {
      while (try_merge_with_next(block));
      
      if (block_is_big_enough(sz, block)) {
        return block_search_result_init(block, BSR_FOUND_GOOD_BLOCK);
      }
    }
    if (block->next != NULL) {
      block = block->next;
    }
  }

  if (block->is_free && block_is_big_enough(sz, block)) {
    return block_search_result_init(block, BSR_FOUND_GOOD_BLOCK);
  }
  
  return block_search_result_init(block, BSR_REACHED_END_NOT_FOUND);
}

 /**
 * Попробовать выделить память в куче начиная с блока `block`, не пытаясь расширить кучу.
 Можно переиспользовать, как только кучу расширили.
 * @param block Начало кучи, @param query_capacity Интересуемая вместимость
 * @return block_search_result
 */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_REACHED_END_NOT_FOUND) {
    return result;
  }
  if (query >= BLOCK_MIN_CAPACITY)
    split_if_too_big(result.block, query);
  result.block->is_free = false;
  return block_search_result_init(result.block, BSR_CORRUPTED);
}

/**
 * @param last Последний элемент старой кучи, @param query Размер расширения кучи
 * @return 
 */
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  void* start_new_region = block_after(last);
  struct region new_region = alloc_region(start_new_region, query);

  if (region_is_invalid(&new_region))
    return NULL;
  
  struct block_header* new_block = (struct block_header*) new_region.addr;
  last->next = new_block;

  if (!try_merge_with_next(last)) {
      return new_block;
  }
  
  return last;
}


static size_t block_actual_capacity( size_t query ) {
  return size_max(BLOCK_MIN_CAPACITY, query);
}
/**
 * Метод реализует основную логику malloc и возвращает заголовок выделенного блока
 * @param heap_start Начало кучи, @param query Запрашиваемый размер блока
 * @return Заголовок выделенного блока
 */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  query = block_actual_capacity(query);
  struct block_search_result mapped_block_result = try_memalloc_existing(query, heap_start);

  if (mapped_block_result.type == BSR_REACHED_END_NOT_FOUND) {
      struct block_header* new_last_block = grow_heap(mapped_block_result.block, query);

      if (!new_last_block) {
        return NULL;
      }

      mapped_block_result = try_memalloc_existing(query, new_last_block);
  }

  return mapped_block_result.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

/**
 * @param contents Указатель на начало содержимого
 * @return Указатель на заголовок блока
 */
static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
